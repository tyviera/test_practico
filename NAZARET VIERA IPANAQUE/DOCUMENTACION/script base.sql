CREATE DATABASE pasajes

USE pasajes 
GO

CREATE TABLE parametro (
                id INT IDENTITY NOT NULL,
                nombre VARCHAR(50) NOT NULL,
                valor VARCHAR(50) NOT NULL,
                CONSTRAINT parametro_pk PRIMARY KEY (id)
)

CREATE TABLE bus (
                id INT IDENTITY NOT NULL,
                placa VARCHAR(15) NOT NULL,
                capacidad INT NOT NULL,
                CONSTRAINT bus_pk PRIMARY KEY (id)
)

CREATE TABLE tipo_documento (
                id INT IDENTITY NOT NULL,
                nombre VARCHAR(50) NOT NULL,
                abreviatura VARCHAR(10) NOT NULL,
                CONSTRAINT tipo_documento_pk PRIMARY KEY (id)
)

CREATE TABLE pasajero (
                id INT NOT NULL,
                nombres VARCHAR(50) NOT NULL,
                apellido_paterno VARCHAR(50) NOT NULL,
                apellido_materno VARCHAR(50) NOT NULL,
                nombre_completo VARCHAR(152) NOT NULL,
                id_tipo_documento INT NOT NULL,
                numero_documento VARCHAR(15) NOT NULL,
                CONSTRAINT pasajero_pk PRIMARY KEY (id)
)

CREATE TABLE categoria (
                id INT IDENTITY NOT NULL,
                nombre VARCHAR(50) NOT NULL,
                precio NUMERIC(9,2) NOT NULL,
                CONSTRAINT categoria_pk PRIMARY KEY (id)
)

CREATE TABLE paradero (
                id INT IDENTITY NOT NULL,
                nombre VARCHAR(50) NOT NULL,
                ubicacion_mapa VARCHAR(2000),
                CONSTRAINT paradero_pk PRIMARY KEY (id)
)

CREATE TABLE pasaje (
                id INT NOT NULL,
                id_parad_origen INT NOT NULL,
                id_parad_dest INT NOT NULL,
                id_pasajero INT NOT NULL,
                id_categoria INT NOT NULL,
                fecha_compra DATETIME NOT NULL,
                numero_boleto VARCHAR(15) NOT NULL,
                ida_vuelta BIT NOT NULL,
                precio_base NUMERIC(9,2) NOT NULL,
                descuentos NUMERIC(9,2) NOT NULL,
                precio_total NUMERIC(9,2) NOT NULL,
                CONSTRAINT pasaje_pk PRIMARY KEY (id)
)

CREATE TABLE tramo (
                id INT NOT NULL,
                id_parad_origen INT NOT NULL,
                id_parad_dest INT NOT NULL,
                precio NUMERIC(9,2) NOT NULL,
                CONSTRAINT tramo_pk PRIMARY KEY (id)
)

CREATE TABLE parada_intermedia (
                id INT NOT NULL,
                id_tramo INT NOT NULL,
                id_bus INT NOT NULL,
                id_pasaje INT NOT NULL,
                CONSTRAINT parada_intermedia_pk PRIMARY KEY (id)
)

ALTER TABLE parada_intermedia ADD CONSTRAINT bus_parada_intermedia_fk
FOREIGN KEY (id_bus)
REFERENCES bus (id)
ON DELETE NO ACTION
ON UPDATE NO ACTION

ALTER TABLE pasajero ADD CONSTRAINT tipo_documento_pasajero_fk
FOREIGN KEY (id_tipo_documento)
REFERENCES tipo_documento (id)
ON DELETE NO ACTION
ON UPDATE NO ACTION

ALTER TABLE pasaje ADD CONSTRAINT pasajero_pasaje_fk
FOREIGN KEY (id_pasajero)
REFERENCES pasajero (id)
ON DELETE NO ACTION
ON UPDATE NO ACTION

ALTER TABLE pasaje ADD CONSTRAINT categoria_pasaje_fk
FOREIGN KEY (id_categoria)
REFERENCES categoria (id)
ON DELETE NO ACTION
ON UPDATE NO ACTION

ALTER TABLE tramo ADD CONSTRAINT paradero_tramo_fk
FOREIGN KEY (id_parad_origen)
REFERENCES paradero (id)
ON DELETE NO ACTION
ON UPDATE NO ACTION

ALTER TABLE tramo ADD CONSTRAINT paradero_tramo_fk1
FOREIGN KEY (id_parad_dest)
REFERENCES paradero (id)
ON DELETE NO ACTION
ON UPDATE NO ACTION

ALTER TABLE pasaje ADD CONSTRAINT paradero_pasaje_fk
FOREIGN KEY (id_parad_dest)
REFERENCES paradero (id)
ON DELETE NO ACTION
ON UPDATE NO ACTION

ALTER TABLE pasaje ADD CONSTRAINT paradero_pasaje_fk1
FOREIGN KEY (id_parad_origen)
REFERENCES paradero (id)
ON DELETE NO ACTION
ON UPDATE NO ACTION

ALTER TABLE parada_intermedia ADD CONSTRAINT pasaje_parada_intermedia_fk
FOREIGN KEY (id_pasaje)
REFERENCES pasaje (id)
ON DELETE NO ACTION
ON UPDATE NO ACTION

ALTER TABLE parada_intermedia ADD CONSTRAINT tramo_parada_intermedia_fk
FOREIGN KEY (id_tramo)
REFERENCES tramo (id)
ON DELETE NO ACTION
ON UPDATE NO ACTION